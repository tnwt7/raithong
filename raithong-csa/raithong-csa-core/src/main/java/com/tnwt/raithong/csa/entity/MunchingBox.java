package com.tnwt.raithong.csa.entity;

import java.util.ArrayList;
import java.util.List;

public class MunchingBox {
	public static final int MAX_ITEM_COUNT = 10;

	public static final String OUTOFRANGE_INDEX_EXCEPTION_MSG = "Index should be in the range 1 - 10."; 
	
	private MunchingBoxSize size;
	private List<Item> items;

	public MunchingBox() {
		this.items = new ArrayList<Item>(MAX_ITEM_COUNT);
		for (int i = 1; i <= MAX_ITEM_COUNT; i++) {
			StringBuilder name = new StringBuilder("name");
			name.append(i);
			Item item = new Item(name.toString());
			this.items.add(item);
		}
	}

	public int getItemCount() {
		return this.items.size();
	}

	public Item getItem(int index) {
		if (index < 0 || index > MAX_ITEM_COUNT-1)
			throw new RuntimeException(OUTOFRANGE_INDEX_EXCEPTION_MSG);
		return this.items.get(index);
	}

	public void setSize(MunchingBoxSize size) {
		this.size = size;
		
	}

	public MunchingBoxSize size() {
		return this.size;
	}
}
